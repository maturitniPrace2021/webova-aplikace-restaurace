-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Stř 14. dub 2021, 22:16
-- Verze serveru: 5.7.17
-- Verze PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `restaurace`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `login`
--

CREATE TABLE `login` (
  `username` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(10) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `login`
--

INSERT INTO `login` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Struktura tabulky `novinky`
--

CREATE TABLE `novinky` (
  `titulek` text COLLATE utf8_czech_ci NOT NULL,
  `obsah` longtext COLLATE utf8_czech_ci NOT NULL,
  `img` text COLLATE utf8_czech_ci NOT NULL,
  `datum` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `novinky`
--

INSERT INTO `novinky` (`titulek`, `obsah`, `img`, `datum`) VALUES
('Nyní máme koktejly!', 'Přijďte si pochutnat na domácích lahodných koktejlech. Máme na výběr mnoho druhů - Blue Lagoon, Mojito, Havana, Bacardi a Angel Face.', 'https://scontent.fprg3-1.fna.fbcdn.net/v/t1.0-9/51746146_112685119863951_2822548388334010368_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=8bfeb9&_nc_ohc=f6PhmGV_fxUAX_H8QeM&_nc_ht=scontent.fprg3-1.fna&oh=7d749d480d09d828ca1c34c5669f0ec7&oe=607FB3E4', '2021-03-18'),
('Užijte si naší zahrádky!', 'Nyní máme otevřenou naší letní zahrádku s obsluhou. Sezení až pro 20 lidí a kolem spousta zeleně. Hudba na místě.', 'https://scontent.fprg3-1.fna.fbcdn.net/v/t1.0-9/118109825_334629401002854_4079567401436028764_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=8bfeb9&_nc_ohc=lFhChDcsOwkAX94DTBf&_nc_ht=scontent.fprg3-1.fna&oh=86da31577eb97bf6ee23f7050ff753a7&oe=607F1EAC', '2020-05-01');

-- --------------------------------------------------------

--
-- Struktura tabulky `produkty`
--

CREATE TABLE `produkty` (
  `id` int(11) NOT NULL,
  `nazev` text COLLATE utf8_czech_ci NOT NULL,
  `cena` int(11) NOT NULL,
  `mnozstvi` int(11) NOT NULL,
  `alergeny` longtext COLLATE utf8_czech_ci NOT NULL,
  `img` text COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `produkty`
--

INSERT INTO `produkty` (`id`, `nazev`, `cena`, `mnozstvi`, `alergeny`, `img`) VALUES
(1, '\r\nGambrinus originál 10°\r\n', 25, 500, '\r\nječmen, oxid siričitý', 'https://vltava.rozhlas.cz/sites/default/files/images/03096151.jpeg'),
(2, '10° Kozel světlý', 25, 500, 'ječmen, oxid siričitý', 'https://www.pivnici.cz/foto/95/d06149022672b4621dd105465e5905b.jpg'),
(3, 'Pilsner Urquell 12°', 35, 500, 'ječmen, oxid siričitý', 'https://www.pivnici.cz/foto/e6/7f1149a9931efcda95023510517b3fb.jpg'),
(4, 'Božkov Vodka', 20, 40, '', 'https://i.pinimg.com/originals/1a/8f/e3/1a8fe3b218b61e2b985c4bed134e2651.jpg'),
(5, 'Božkov Rum', 20, 40, '', 'https://i.pinimg.com/originals/53/f8/f2/53f8f29967dddf2fabc12b23d67c6cfe.png'),
(6, 'Božkov Peprmint', 20, 40, '', 'https://www.bozkov.cz/sites/default/files/2020-04/panak_peprmint.png'),
(7, 'Finlandia ', 30, 40, '', 'https://i.pinimg.com/originals/1a/8f/e3/1a8fe3b218b61e2b985c4bed134e2651.jpg'),
(8, 'Absolut Vodka', 30, 40, '', 'https://i.pinimg.com/originals/1a/8f/e3/1a8fe3b218b61e2b985c4bed134e2651.jpg'),
(9, 'Jack Daniel’s', 35, 40, 'bourbon', 'https://barshopen.com/images/normal/jack-daniels-shotglas-full.jpg'),
(10, 'Jack Daniel’s Fire', 35, 40, 'bourbon', 'https://barshopen.com/images/normal/jack-daniels-shotglas-full.jpg'),
(11, 'Rum Captain Morgan', 30, 40, '', 'https://i.pinimg.com/originals/53/f8/f2/53f8f29967dddf2fabc12b23d67c6cfe.png'),
(12, 'Sierra Tequila', 35, 40, '', 'https://static6.depositphotos.com/1000970/572/i/600/depositphotos_5720204-stock-photo-tequila.jpg'),
(13, 'Bohemia Sekt', 50, 370, '', 'https://www.osobnivinoteka.cz/media/image/thumb/1/bs_demisec_320x800.jpg/p_max_size.jpg'),
(14, 'Fernet', 30, 40, '', 'https://secure.ce-tescoassets.com/assets/CZ/115/8594005020115/ShotType1_540x540.jpg'),
(15, 'Bílé víno', 40, 370, '', 'https://www.mall.cz/i/42492671/550/550'),
(16, 'Červené víno', 40, 370, '', 'https://i.cdn.nrholding.net/42492689/550/550'),
(17, 'Jahodový džus', 20, 250, '', 'https://envato-shoebox-0.imgix.net/0bc1/ff64-b049-4a26-a000-2115f0f34e26/20+-+24.04.2016.jpg?auto=compress%2Cformat&fit=max&mark=https%3A%2F%2Felements-assets.envato.com%2Fstatic%2Fwatermark2.png&markalign=center%2Cmiddle&markalpha=18&w=1600&s=76c4b2344f6b318a8035f5fc631227c0'),
(18, 'Pomerančový džus', 20, 250, '', 'https://obchod.darjaninsoftware.cz/wp-content/uploads/sites/3/2019/02/oj-upgrade-103121806_horiz_0.jpg'),
(19, 'Capuccino', 35, 200, '', 'https://cz.jura.com/-/media/global/images/coffee-recipes/images-redesign-2020/cappuccino_2000x1400px.jpg?h=1400&la=cs&w=2000&hash=AD9880D30498282B2CC3496FBFEBE9C5EDA1B46F'),
(20, 'Espresso', 35, 200, '', 'https://cdn.myshoptet.com/usr/www.mojeprazirna.cz/user/shop/big/265-1_coffee-now-hrnek-espresso-1.jpg?58b05aab'),
(21, 'Espresso lungo', 35, 200, '', 'https://cdn.myshoptet.com/usr/www.mojeprazirna.cz/user/shop/big/265-1_coffee-now-hrnek-espresso-1.jpg?58b05aab'),
(22, 'Vietnamská káva', 40, 200, '', 'https://vietnamista.cz/_files/system_preview_detail_200017899-0903d09ff7/kava7.jpg'),
(23, 'Nakládaný hermelín', 40, 200, '', 'https://slevomat.sgcdn.cz/images/t/2000/10/25/10253006-def77b.jpg'),
(24, 'Utopenec', 40, 200, '', 'https://1gr.cz/fotky/idnes/10/063/cl5/ABR33fb2e_25ONA29a.jpg'),
(25, 'Hranolky', 40, 250, '', 'https://1gr.cz/fotky/lidovky/13/082/lnc460/APE4d39d3_hranolky.jpg'),
(40, 'Pizza Mafia', 110, 300, 'obiloviny, lepek', 'https://lh3.googleusercontent.com/proxy/9CNLqye4wl8napGqc4GaeyfggRQ9-f-JLaGi6iI8Wdw1LkRwpU1MAa1gGfJK1fb0x1MRGosAcWInWMiuAGByJcKy7ZP9fOA3k3r0dIrh0an7mJ_FSpBKTiAU9C8hdgE');

-- --------------------------------------------------------

--
-- Struktura tabulky `rezervace`
--

CREATE TABLE `rezervace` (
  `id` int(1) NOT NULL,
  `jmeno` text COLLATE utf8_czech_ci NOT NULL,
  `tel` text COLLATE utf8_czech_ci NOT NULL,
  `datum` date NOT NULL,
  `cas` time NOT NULL,
  `osoby` text COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `zpravy`
--

CREATE TABLE `zpravy` (
  `jmeno` text COLLATE utf8_czech_ci NOT NULL,
  `zprava` longtext COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `zpravy`
--

INSERT INTO `zpravy` (`jmeno`, `zprava`) VALUES
('', ''),


--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `produkty`
--
ALTER TABLE `produkty`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `rezervace`
--
ALTER TABLE `rezervace`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `produkty`
--
ALTER TABLE `produkty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT pro tabulku `rezervace`
--
ALTER TABLE `rezervace`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
