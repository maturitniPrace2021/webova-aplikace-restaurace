<?php
require_once("vendor/autoload.php");
$latte = new Latte\Engine;

$latte->setTempDirectory('temp');

include "config.php";

$sqlConn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);

$sqlConn->set_charset("utf8");

if ($sqlConn->connect_error) {
  die("Připojení selhalo: " . $sqlConn->connect_error);
}
$sql = "SELECT titulek,obsah,img,datum FROM novinky";
$result = $sqlConn->query($sql);
if ($result->num_rows > 0 ){
  while ($row = $result->fetch_assoc()){
    $novinky[]=array($row{"titulek"},$row{"obsah"},$row{"img"},$row{"datum"});
  }
  }
  $params = [
    "novinky" => $novinky,

  ];


  $latte->render('template/HlavniStranka.latte', $params);
?>
