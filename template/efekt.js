(() => {
    let card = document.getElementById('card');
    let topCard = card.offsetTop;
    let leftCard = card.offsetLeft;
    let widthCard = card.clientWidth;
    let heightCard = card.clientHeight;
  
    card.addEventListener('mousemove', function (e) {
      let x = e.clientX - leftCard;
      let y = e.clientY - topCard;
      let teta = 5;
      let degX = ((x / (widthCard/2))*5) - 5;
      let degY = ((y / (heightCard/2))*5) - 5;
      // console.log((degX) + ',' + (-degY));
      //console.log('(' + x + ',' + y + ')');
      if(x <= widthCard/2 && y <= heightCard/2) {
        // card.setAttribute('style', `transform: rotateY(${-degX}deg) rotateX(${degY}deg);`);
        card.setAttribute('style', `transform: rotateY(${degX}deg) rotateX(${-degY}deg);`);
      }
      if(x <= widthCard/2 && y > heightCard/2) {
        // card.setAttribute('style', `transform: rotateY(${-degX}deg) rotateX(${degY}deg);`);
        card.setAttribute('style', `transform: rotateY(${degX}deg) rotateX(${-degY}deg);`);
        //console.log(card);
      }
      if(x > widthCard/2 && y <= heightCard/2) {
        // card.setAttribute('style', `transform: rotateY(${-degX}deg) rotateX(${degY}deg);`);
        card.setAttribute('style', `transform: rotateY(${degX}deg) rotateX(${-degY}deg);`);
      }
      if(x > widthCard/2 && y > heightCard/2){
        // card.setAttribute('style', `transform: rotateY(${-degX}deg) rotateX(${degY}deg);`);
        card.setAttribute('style', `transform: rotateY(${degX}deg) rotateX(${-degY}deg);`);
      }
    });
    
    card.addEventListener('mouseout', function () {
      card.setAttribute('style', `transform: rotateY(0) rotateX(0);`);
      });
  })();