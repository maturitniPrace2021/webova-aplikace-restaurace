<?php
require_once("../vendor/autoload.php");
$latte = new Latte\Engine;

$latte->setTempDirectory('../temp');

include "../config.php";

$sqlConn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);

$sqlConn->set_charset("utf8");
if ($sqlConn->connect_error) {
  die("Připojení selhalo: " . $sqlConn->connect_error);
}
class produkt {
  private $nazev;
  private $cena;
  private $mnozstvi;
  private $alergeny;
  private $img;


public function setNazev($nazev){
$this -> nazev = $nazev  ;
}
public function getNazev(){
return $this -> nazev;
}

public function setCena($cena){
$this -> cena = $cena;
}
public function getCena(){
return $this -> cena;
}
public function setMnozstvi($mnozstvi){
$this -> mnozstvi = $mnozstvi ;
}
public function getMnozstvi(){
return $this -> mnozstvi;
}
public function setAlergeny($alergeny){
$this -> alergeny = $alergeny ;
}
public function getAlergeny(){
return $this -> alergeny;
}
public function setImg($img){
$this -> img = $img;
}
public function getImg(){
return $this -> img;
}
}

$produkt = new produkt();

if(isset($_POST["pridatprodukt"])){
    $produkt->setNazev($_POST['nazev']);
    $produkt->setCena($_POST['cena']);
    $produkt->setMnozstvi($_POST['mnozstvi']);
    $produkt->setAlergeny($_POST['alergeny']);
    $produkt->setImg($_POST['img']);
}


$sql = "INSERT INTO produkty (nazev, cena, mnozstvi, alergeny, img) VALUES ('".$produkt->getNazev()."', '".$produkt->getCena()."', '".$produkt->getMnozstvi()."','".$produkt->getAlergeny()."', '".$produkt->getImg()."')";


if ($sqlConn->query($sql) === TRUE) {
    echo "Produkt byl přidán";
} else {
     "Error: " . $sql . "<br>" . $sqlConn->error;
}

class novinka {
  private $titulek;
  private $obsah;
  private $img;
  private $datum;



public function setTitulek($titulek){
$this -> titulek = $titulek  ;
}
public function getTitulek(){
return $this -> titulek;
}

public function setObsah($obsah){
$this -> obsah = $obsah;
}
public function getObsah(){
return $this -> obsah;
}
public function setImg($img){
$this -> img = $img ;
}
public function getImg(){
return $this -> img;
}
public function setDatum($datum){
$this -> datum = $datum ;
}
public function getDatum(){
return $this -> datum;
}
}

$novinka = new novinka();

if(isset($_POST["pridatnovinku"])){
    $novinka->setTitulek($_POST['titulek']);
    $novinka->setObsah($_POST['obsah']);
    $novinka->setImg($_POST['img']);
    $novinka->setDatum($_POST['datum']);

}


$sql = "INSERT INTO novinky (titulek, obsah, img, datum) VALUES ('".$novinka->getTitulek()."', '".$novinka->getObsah()."', '".$novinka->getImg()."','".$novinka->getDatum()."')";


if ($sqlConn->query($sql) === TRUE) {
    echo "Novinka byla přidána";
} else {
    "Error: " . $sql . "<br>" . $sqlConn->error;
}
$sql = "SELECT id,jmeno,tel,datum,cas,osoby FROM rezervace";
$result = $sqlConn->query($sql);
if ($result->num_rows > 0 ){
  while ($row = $result->fetch_assoc()){
    $rezervace[]=array($row{"id"},$row{"jmeno"},$row{"tel"},$row{"datum"},$row{"cas"},$row{"osoby"});
  }
}


$params = [
  "rezervace" => $rezervace,

];

          if(isset($_GET["id"])){
          $id=$_GET["id"];
          $sql = "DELETE FROM rezervace WHERE id=$id";


    if ($sqlConn->query($sql) === TRUE) {
        echo "Rezervace byla zrušena";
    } else {
        echo "Error: " . $sql . "<br>" . $sqlConn->error;
      }
      header("location: welcome.php");
        }


  $latte->render('../template/welcome.latte', $params);


?>
