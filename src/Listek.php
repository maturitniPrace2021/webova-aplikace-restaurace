<?php
require_once("../vendor/autoload.php");
$latte = new Latte\Engine;

$latte->setTempDirectory('../temp');

include "../config.php";

$sqlConn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);

$sqlConn->set_charset("utf8");

if ($sqlConn->connect_error) {
  die("Připojení selhalo: " . $sqlConn->connect_error);
}
$sql = "SELECT nazev,cena,mnozstvi,alergeny,img FROM produkty";
$result = $sqlConn->query($sql);
if ($result->num_rows > 0 ){
  while ($row = $result->fetch_assoc()){
    $produkty[]=array($row{"nazev"},$row{"cena"},$row{"mnozstvi"},$row{"alergeny"},$row{"img"});
  }
}


$params = [
  "produkty" => $produkty,

];


  $latte->render('../template/Listek.latte', $params);
?>
