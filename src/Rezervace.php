<?php
require_once("../vendor/autoload.php");
$latte = new Latte\Engine;

$latte->setTempDirectory('../temp');

include "../config.php";

$sqlConn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);

$sqlConn->set_charset("utf8");

if ($sqlConn->connect_error) {
  die("Připojení selhalo: " . $sqlConn->connect_error);
}


$alert="";

class rezervace {
  private $jmeno;
  private $tel;
  private $datum;
  private $cas;
  private $osoby;


public function setJmeno($jmeno){
$this -> jmeno = $jmeno  ;
}
public function getJmeno(){
return $this -> jmeno;
}

public function setTel($tel){
$this -> tel = $tel;
}
public function getTel(){
return $this -> tel;
}
public function setDatum($datum){
$this -> datum = $datum ;
}
public function getDatum(){
return $this -> datum;
}
public function setCas($cas){
$this -> cas = $cas ;
}
public function getCas(){
return $this -> cas;
}
public function setOsoby($osoby){
$this -> osoby = $osoby;
}
public function getOsoby(){
return $this -> osoby;
}
}

$rezervace = new rezervace();

if(isset($_POST["rezervovat"])){
    $rezervace->setJmeno($_POST['jmeno']);
    $rezervace->setTel($_POST['tel']);
    $rezervace->setDatum($_POST['datum']);
    $rezervace->setCas($_POST['cas']);
    $rezervace->setOsoby($_POST['osoby']);
}


$sql = "INSERT INTO rezervace (jmeno, tel, datum, cas, osoby) VALUES ('".$rezervace->getJmeno()."', '".$rezervace->getTel()."', '".$rezervace->getDatum()."','".$rezervace->getCas()."', '".$rezervace->getOsoby()."')";
$rezervovat=isset($_POST["rezervovat"]);
if ($rezervovat){
if ($sqlConn->query($sql) === TRUE) {

      $alert="Rezervace byla vytvořena";
    } else {
       $alert= "Datum a čas musí být vyplněn";
    }
    }
    $sqlConn->close();


    $params = [
      'alert' => $alert,
    ];



  $latte->render('../template/Rezervace.latte',$params);
?>
