<?php
// source: welcome.php

use Latte\Runtime as LR;

class Template1ba7503ed9 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
		
		require_once("../vendor/autoload.php");
		$latte = new Latte\Engine;

		$latte->setTempDirectory('../temp');

		include "../config.php";
		include "session.php";


		$latte->render('../template/welcome.latte');
?>

<html>

   <head>
      <title> Vítejte </title>
   </head>

   <body>
      <h1> Vítejte <?php
		echo $login_session;
?></h1>
      <h2><a href = "logout.php"> Odhlásit se </a></h2>
   </body>

</html>
<?php
		return get_defined_vars();
	}

}
