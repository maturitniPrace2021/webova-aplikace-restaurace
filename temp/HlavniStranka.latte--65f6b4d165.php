<?php
// source: template/HlavniStranka.latte

use Latte\Runtime as LR;

class Template65f6b4d165 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="template/HlavniStranka.css">

</head>
<body>
	<header>
		<a href="index.php">
		<img src="template/logo.png">
		
		</a>
		<nav>
			<ul class="nav__links">
				<li><a href="src/Rezervace.php">REZERVACE</a></li>
				<li><a href="src/Listek.php">LÍSTEK</a></li>
				<li><a href="src/Kontakty.php">KONTAKT</a></li>
			</ul>

		</nav>
		<a href="template/admin.latte"><button type="submit" name="admin" class="submit">Admin</button></a>

	</header>

	<div class="flex-container">
	<div class="flex-item-left">

    </div>

		<div class="flex-item-mid">
			<div class="koktejly">
					<h2> <?php echo LR\Filters::escapeHtmlText($novinky[0][0]) /* line 33 */ ?> </h2>
					<img id="myImg" src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($novinky[0][2])) /* line 34 */ ?>" alt="<?php
		echo LR\Filters::escapeHtmlAttr($novinky[0][1]) /* line 34 */ ?>">  </img>
					<p> <?php echo LR\Filters::escapeHtmlText($novinky[0][1]) /* line 35 */ ?> </p>
           <p> <?php echo LR\Filters::escapeHtmlText($novinky[0][3]) /* line 36 */ ?> </p>
					</div>
					<div class="zahradka">
							<h2> <?php echo LR\Filters::escapeHtmlText($novinky[1][0]) /* line 39 */ ?> </h2>
							<img id="myImg1" src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($novinky[1][2])) /* line 40 */ ?>" alt="<?php
		echo LR\Filters::escapeHtmlAttr($novinky[1][1]) /* line 40 */ ?>">  </img>
							<p> <?php echo LR\Filters::escapeHtmlText($novinky[1][1]) /* line 41 */ ?> </p>
		           <p> <?php echo LR\Filters::escapeHtmlText($novinky[1][3]) /* line 42 */ ?> </p>

							</div>
							</div>
	<div class="flex-item-right">
	</div>


</div>
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
<div id="myModal1" class="modal1">
  <span class="close1">&times;</span>
  <img class="modal-content1" id="img02">
  <div id="caption1"></div>
</div>
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal1");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg1");
var modalImg = document.getElementById("img02");
var captionText = document.getElementById("caption1");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close1")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
	<footer>
	<div class="zakaz">
		Zákaz prodeje alkoholu osobám mladším 18 let.
		</div>
	</footer>
	
</body>
</html>
<?php
		return get_defined_vars();
	}

}
