<?php
// source: ../template/Listek.latte

use Latte\Runtime as LR;

class Template347b3c5db5 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="../template/Listek.css">

</head>
<body>
	<header>
        <a href="../index.php">
		<img class="logo" src="../template/logo.png" alt="logo">
        </a>
		<nav>
			<ul class="nav__links">
				<li><a href="../src/Rezervace.php">REZERVACE</a></li>
				<li><a href="../src/Listek.php">LÍSTEK</a></li>
				<li><a href="../src/Kontakty.php">KONTAKT</a></li>
			</ul>
		</nav>

	</header>
	<div class="flex-container">
	<div class="flex-item-left">

    </div>
    <div class="wrapper">
        <div class="title">
            <h4><span>Monika Bar</span>Jídelní Lístek</h4>
        </div>
        <div class="title">
            <h4><span>Alkoholické</span>Nápoje</h4>
        </div>
        <div class="border2">
        <div class="border">
        <div class="alko">
            <div class="single-menu">
                <img  id="myImg" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[0][0]) /* line 37 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[0][4])) /* line 37 */ ?>">  </img>
                <div class="menu-content">
                    <h4> <?php echo LR\Filters::escapeHtmlText($produkty[0][0]) /* line 39 */ ?>   <?php
		echo LR\Filters::escapeHtmlText($produkty[0][1]) /* line 39 */ ?>,- Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[0][2]) /* line 40 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[0][3]) /* line 40 */ ?> </p>
                </div>
            </div>
            <div class="single-menu">
                <img  id="myImg1" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[1][0]) /* line 44 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[1][4])) /* line 44 */ ?>"></img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[1][0]) /* line 46 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[1][1]) /* line 46 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[1][2]) /* line 47 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[1][3]) /* line 47 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg2" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[2][0]) /* line 51 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[2][4])) /* line 51 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[2][0]) /* line 53 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[2][1]) /* line 53 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[2][2]) /* line 54 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[2][3]) /* line 54 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg3" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[3][0]) /* line 58 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[3][4])) /* line 58 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[3][0]) /* line 60 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[3][1]) /* line 60 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[3][2]) /* line 61 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[3][3]) /* line 61 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg4" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[4][0]) /* line 65 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[4][4])) /* line 65 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[4][0]) /* line 67 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[4][1]) /* line 67 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[4][2]) /* line 68 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[4][3]) /* line 68 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg5" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[5][0]) /* line 72 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[5][4])) /* line 72 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[5][0]) /* line 74 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[5][1]) /* line 74 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[5][2]) /* line 75 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[5][3]) /* line 75 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg6" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[6][0]) /* line 79 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[6][4])) /* line 79 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[6][0]) /* line 81 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[6][1]) /* line 81 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[6][2]) /* line 82 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[6][3]) /* line 82 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg7" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[7][0]) /* line 86 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[7][4])) /* line 86 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[7][0]) /* line 88 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[7][1]) /* line 88 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[7][2]) /* line 89 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[7][3]) /* line 89 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg8" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[8][0]) /* line 93 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[8][4])) /* line 93 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[8][0]) /* line 95 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[8][1]) /* line 95 */ ?>,-Kč </h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[8][2]) /* line 96 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[8][3]) /* line 96 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg9" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[9][0]) /* line 100 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[9][4])) /* line 100 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[9][0]) /* line 102 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[9][1]) /* line 102 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[9][2]) /* line 103 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[9][3]) /* line 103 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg10" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[10][0]) /* line 107 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[10][4])) /* line 107 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[10][0]) /* line 109 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[10][1]) /* line 109 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[10][2]) /* line 110 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[10][3]) /* line 110 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg11" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[11][0]) /* line 114 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[11][4])) /* line 114 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[11][0]) /* line 116 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[11][1]) /* line 116 */ ?>,-Kč </h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[11][2]) /* line 117 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[11][3]) /* line 117 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg12" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[12][0]) /* line 121 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[12][4])) /* line 121 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[12][0]) /* line 123 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[12][1]) /* line 123 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[12][2]) /* line 124 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[12][3]) /* line 124 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg13" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[13][0]) /* line 128 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[13][4])) /* line 128 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[13][0]) /* line 130 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[13][1]) /* line 130 */ ?>,-Kč </h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[13][2]) /* line 131 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[13][3]) /* line 131 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg14" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[14][0]) /* line 135 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[14][4])) /* line 135 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[14][0]) /* line 137 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[14][1]) /* line 137 */ ?>,-Kč </h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[14][2]) /* line 138 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[14][3]) /* line 138 */ ?></p>
                </div>
            </div>

            <div class="single-menu">
                <img id="myImg15" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[15][0]) /* line 143 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[15][4])) /* line 143 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[15][0]) /* line 145 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[15][1]) /* line 145 */ ?>,-Kč </h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[15][2]) /* line 146 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[15][3]) /* line 146 */ ?></p>
                </div>
            </div>
        </div>
        <div class="nealko">
            <div class="title">
                <h4><span>Nealkoholické</span>Nápoje</h4>
            </div>
            <div class="single-menu">
                <img id="myImg16" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[16][0]) /* line 155 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[16][4])) /* line 155 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[16][0]) /* line 157 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[16][1]) /* line 157 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[16][2]) /* line 158 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[16][3]) /* line 158 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg17" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[17][0]) /* line 162 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[17][4])) /* line 162 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[17][0]) /* line 164 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[17][1]) /* line 164 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[17][2]) /* line 165 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[17][3]) /* line 165 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg18" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[18][0]) /* line 169 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[18][4])) /* line 169 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[18][0]) /* line 171 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[18][1]) /* line 171 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[18][2]) /* line 172 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[18][3]) /* line 172 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg19" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[19][0]) /* line 176 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[19][4])) /* line 176 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[19][0]) /* line 178 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[19][1]) /* line 178 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[19][2]) /* line 179 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[19][3]) /* line 179 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg20" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[20][0]) /* line 183 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[20][4])) /* line 183 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[20][0]) /* line 185 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[20][1]) /* line 185 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[20][2]) /* line 186 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[20][3]) /* line 186 */ ?></p>
                </div>
            </div>
              <div class="single-menu">
                <img id="myImg22" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[22][0]) /* line 190 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[22][4])) /* line 190 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[22][0]) /* line 192 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[22][1]) /* line 192 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[22][2]) /* line 193 */ ?> g <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[22][3]) /* line 193 */ ?></p>
                </div>
            </div>
        </div>
        <div class="jidlo">
            <div class="title">
                <h4><span>Teplá</span>jídla</h4>
            </div>
           <div class="single-menu">
                <img id="myImg21" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[21][0]) /* line 202 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[21][4])) /* line 202 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[21][0]) /* line 204 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[21][1]) /* line 204 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[21][2]) /* line 205 */ ?> ml <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[21][3]) /* line 205 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg23" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[23][0]) /* line 209 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[23][4])) /* line 209 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[23][0]) /* line 211 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[23][1]) /* line 211 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[23][2]) /* line 212 */ ?> g <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[23][3]) /* line 212 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg24" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[24][0]) /* line 216 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[24][4])) /* line 216 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[24][0]) /* line 218 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[24][1]) /* line 218 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[24][2]) /* line 219 */ ?> g <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[24][3]) /* line 219 */ ?></p>
                </div>
            </div>
            <div class="single-menu">
                <img id="myImg25" alt="<?php echo LR\Filters::escapeHtmlAttr($produkty[25][0]) /* line 223 */ ?>" src="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($produkty[25][4])) /* line 223 */ ?>">  </img>
                <div class="menu-content">
                    <h4><?php echo LR\Filters::escapeHtmlText($produkty[25][0]) /* line 225 */ ?>  <?php
		echo LR\Filters::escapeHtmlText($produkty[25][1]) /* line 225 */ ?>,-Kč</h4>
                    <p><?php echo LR\Filters::escapeHtmlText($produkty[25][2]) /* line 226 */ ?> g <br>  <?php
		echo LR\Filters::escapeHtmlText($produkty[25][3]) /* line 226 */ ?></p>
                </div>
            </div>
        </div>
        </div>
        </div>
</div>
    </div>
	<div class="flex-item-right">

	</div>


</div>
</div>
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
<div id="myModal1" class="modal1">
  <span class="close1">&times;</span>
  <img class="modal-content1" id="img02">
  <div id="caption1"></div>
</div>
<div id="myModal2" class="modal2">
  <span class="close2">&times;</span>
  <img class="modal-content2" id="img03">
  <div id="caption2"></div>
</div>
<div id="myModal3" class="modal3">
  <span class="close3">&times;</span>
  <img class="modal-content3" id="img04">
  <div id="caption3"></div>
</div>
<div id="myModal4" class="modal4">
  <span class="close4">&times;</span>
  <img class="modal-content4" id="img05">
  <div id="caption4"></div>
</div>
<div id="myModal5" class="modal5">
  <span class="close5">&times;</span>
  <img class="modal-content5" id="img06">
  <div id="caption5"></div>
</div>
<div id="myModal6" class="modal6">
  <span class="close6">&times;</span>
  <img class="modal-content6" id="img07">
  <div id="caption6"></div>
</div>
<div id="myModal7" class="modal7">
  <span class="close7">&times;</span>
  <img class="modal-content7" id="img08">
  <div id="caption7"></div>
</div>
<div id="myModal8" class="modal8">
  <span class="close8">&times;</span>
  <img class="modal-content8" id="img09">
  <div id="caption8"></div>
</div>
<div id="myModal9" class="modal9">
  <span class="close9">&times;</span>
  <img class="modal-content9" id="img10">
  <div id="caption9"></div>
</div>
<div id="myModal10" class="modal10">
  <span class="close10">&times;</span>
  <img class="modal-content10" id="img11">
  <div id="caption10"></div>
</div>
<div id="myModal11" class="modal11">
  <span class="close11">&times;</span>
  <img class="modal-content11" id="img12">
  <div id="caption11"></div>
</div>
<div id="myModal12" class="modal12">
  <span class="close12">&times;</span>
  <img class="modal-content12" id="img13">
  <div id="caption12"></div>
</div>
<div id="myModal13" class="modal13">
  <span class="close13">&times;</span>
  <img class="modal-content13" id="img14">
  <div id="caption13"></div>
</div>
<div id="myModal14" class="modal14">
  <span class="close14">&times;</span>
  <img class="modal-content14" id="img15">
  <div id="caption14"></div>
</div>
<div id="myModal15" class="modal15">
  <span class="close15">&times;</span>
  <img class="modal-content15" id="img16">
  <div id="caption15"></div>
</div>
<div id="myModal16" class="modal16">
  <span class="close16">&times;</span>
  <img class="modal-content16" id="img17">
  <div id="caption16"></div>
</div>
<div id="myModal17" class="modal17">
  <span class="close17">&times;</span>
  <img class="modal-content17" id="img18">
  <div id="caption17"></div>
</div>
<div id="myModal18" class="modal18">
  <span class="close18">&times;</span>
  <img class="modal-content18" id="img19">
  <div id="caption18"></div>
</div>
<div id="myModal19" class="modal19">
  <span class="close19">&times;</span>
  <img class="modal-content19" id="img20">
  <div id="caption19"></div>
</div>
<div id="myModal20" class="modal20">
  <span class="close20">&times;</span>
  <img class="modal-content20" id="img21">
  <div id="caption20"></div>
</div>
<div id="myModal21" class="modal21">
  <span class="close21">&times;</span>
  <img class="modal-content21" id="img22">
  <div id="caption21"></div>
</div>
<div id="myModal22" class="modal22">
  <span class="close22">&times;</span>
  <img class="modal-content22" id="img23">
  <div id="caption22"></div>
</div>
<div id="myModal23" class="modal23">
  <span class="close23">&times;</span>
  <img class="modal-content23" id="img24">
  <div id="caption23"></div>
</div>
<div id="myModal24" class="modal24">
  <span class="close">&times;</span>
  <img class="modal-content24" id="img25">
  <div id="caption24"></div>
</div>
<div id="myModal25" class="modal25">
  <span class="close25">&times;</span>
  <img class="modal-content25" id="img26">
  <div id="caption25"></div>
</div>
<script>
// Get the modal
var modal = document.getElementById("myModal",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg",);
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal1",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg1",);
var modalImg = document.getElementById("img02");
var captionText = document.getElementById("caption1");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal1")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal2",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg2",);
var modalImg = document.getElementById("img03");
var captionText = document.getElementById("caption2");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal3",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg3",);
var modalImg = document.getElementById("img04");
var captionText = document.getElementById("caption3");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal3")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal4",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg4",);
var modalImg = document.getElementById("img05");
var captionText = document.getElementById("caption4");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal4")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal5",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg5",);
var modalImg = document.getElementById("img06");
var captionText = document.getElementById("caption5");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal5")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal6",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg6",);
var modalImg = document.getElementById("img07");
var captionText = document.getElementById("caption6");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal6")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal7",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg7",);
var modalImg = document.getElementById("img08");
var captionText = document.getElementById("caption7");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal7")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal8",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg8",);
var modalImg = document.getElementById("img09");
var captionText = document.getElementById("caption8");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal8")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal9",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg9",);
var modalImg = document.getElementById("img10");
var captionText = document.getElementById("caption9");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal9")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal10",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg10",);
var modalImg = document.getElementById("img11");
var captionText = document.getElementById("caption10");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal10")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal11",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg11",);
var modalImg = document.getElementById("img12");
var captionText = document.getElementById("caption11");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal11")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal12",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg12",);
var modalImg = document.getElementById("img13");
var captionText = document.getElementById("caption12");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal12")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal13",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg13",);
var modalImg = document.getElementById("img14");
var captionText = document.getElementById("caption13");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal13")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal14",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg14",);
var modalImg = document.getElementById("img15");
var captionText = document.getElementById("caption14");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal14")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal15",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg15",);
var modalImg = document.getElementById("img16");
var captionText = document.getElementById("caption15");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal15")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal16",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg16",);
var modalImg = document.getElementById("img17");
var captionText = document.getElementById("caption16");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal16")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal17",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg17",);
var modalImg = document.getElementById("img18");
var captionText = document.getElementById("caption17");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal17")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal18",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg18",);
var modalImg = document.getElementById("img19");
var captionText = document.getElementById("caption18");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal18")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal19",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg19",);
var modalImg = document.getElementById("img20");
var captionText = document.getElementById("caption19");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal19")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal20",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg20",);
var modalImg = document.getElementById("img21");
var captionText = document.getElementById("caption20");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal20")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal21",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg21",);
var modalImg = document.getElementById("img22");
var captionText = document.getElementById("caption21");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal21")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal22",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg22",);
var modalImg = document.getElementById("img23");
var captionText = document.getElementById("caption22");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal22")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal23",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg23",);
var modalImg = document.getElementById("img24");
var captionText = document.getElementById("caption23");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal23")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal24",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg24",);
var modalImg = document.getElementById("img25");
var captionText = document.getElementById("caption24");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal24")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal25",);

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg25",);
var modalImg = document.getElementById("img26");
var captionText = document.getElementById("caption25");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("modal25")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
	<footer>
    <div class="zakaz">
		Zákaz prodeje alkoholu osobám mladším 18 let.
        </div>
	</footer>
</body>
</html>
<?php
		return get_defined_vars();
	}

}
