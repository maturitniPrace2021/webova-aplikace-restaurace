<?php
// source: ../template/Novinky.latte

use Latte\Runtime as LR;

class Template9b4e30d9b3 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="../template/Novinky.css">

</head>
<body>
	<header>
		<img class="logo" src="../template/logo.png" alt="logo">
		<nav>
			<ul class="nav__links">
				<li><a href="../src/Rezervace.php">REZERVACE</a></li>
				<li><a href="../src/Listek.php">LÍSTEK</a></li>
				<li><a href="../src/Novinky.php">NOVINKY</a></li>
				<li><a href="../src/Kontakty.php">KONTAKT</a></li>
			</ul>
		</nav>
	</header>
	<div class="flex-container">
	<div class="flex-item-left">

    </div>
    <div class="flex-item-mid">
    </div>
	<div class="flex-item-right">

	</div>


</div>
	<footer>
	</footer>
</body>
</html>
<?php
		return get_defined_vars();
	}

}
