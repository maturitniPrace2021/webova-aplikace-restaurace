<?php
// source: template/Lístek.latte

use Latte\Runtime as LR;

class Template5fbd6321dd extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="template/Lístek.css">

</head>
<body>
	<header>
		<img class="logo" src="template/logo.png" alt="logo">
		<nav>
			<ul class="nav__links">
				<li><a href="src/Rezervace.php">REZERVACE</a></li>
				<li><a href="src/Lístek.php">LÍSTEK</a></li>
				<li><a href="src/Novinky.php">NOVINKY</a></li>
				<li><a href="src/Kontakt.php">KONTAKT</a></li>
			</ul>
		</nav>
		<a class="cta" href="src/Admin.php"><button class="button">PŘIHLÁSIT SE</button></a>
	</header>
	<div class="flex-container">

	<div class="flex-item-left">
		<img class="banner" alt="banner" src="template/banner.jpg">
    </div>
    <div class="flex-item-mid">
<p>
	<?php echo LR\Filters::escapeHtmlText($flexitemmid["nazev"]) /* line 28 */ ?>

</p>

    </div>
	<div class="flex-item-right">
		<img class="banner1" alt="banner1" src="template/banner1.jpg">
	</div>


</div>
	<footer>
	</footer>
</body>
</html>
<?php
		return get_defined_vars();
	}

}
