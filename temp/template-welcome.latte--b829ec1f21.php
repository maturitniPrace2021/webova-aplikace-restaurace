<?php
// source: ../template/welcome.latte

use Latte\Runtime as LR;

class Templateb829ec1f21 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="../template/welcome.css">

</head>
<body>
	<header>

	</header>
    <h2>Přidání produktu do lístku</h2>
    <div class="pridani">
	<form action="../src/welcome.php" method="POST" name="produktu" autocomplete="on">
    <div class="form-field">
        <input type="text" name="nazev" placeholder="nazev" id="nazev">
        <input type="number" name="cena" placeholder="cena" id="cena">Kč
        <input type="number" name="mnozstvi" placeholder="mnozstvi" id="mnozstvi">g/ml
        <input type="text" name="alergeny" placeholder="alergeny" id="alergeny">
        <input type="text" name="img" placeholder="img" id="img">url
    </div>
    <button  type="submit" name="pridatprodukt" class="submit">Přidat produkt</button>
	</div>
    </form>
		<h2>Přidání novinek</h2>
		<div class="pridaninovinek">
		<form action="../src/welcome.php" method="POST" name="novinky" autocomplete="on">
	    <div class="form-field">
	        <input type="text" name="titulek" placeholder="titulek" id="titulek">
	        <input type="text" name="obsah" placeholder="obsah" id="obsah">
	        <input type="text" name="img" placeholder="img" id="img">URL
	        <input type="date" name="datum" placeholder="datum" id="datum">
	    </div>
	    <button  type="submit" name="pridatnovinku" class="submit">Přidat novinku</button>

	    </form>
			</div>
    </div>
	
    <h2>Zrušení rezervace</h2>
    <div class="zruseni">
    <form action="../src/welcome.php" method="GET" name="zruseni" >
    <div class="form-field">

        <input type="number" name="id" placeholder="id" id="id" min="0" required>
    <button  type="submit" name="zrusit" class="submit">Zrušit rezervaci</button>
		</div>
    </form>
    </div>
	<div class="scroll">
	<table id="rezervace">
    <tr id="tabulka">
      <th>ID:</th>
      <th>Jméno:</th>
      <th>Tel:</th>
      <th>Datum:</th>
	  <th>Čas:</th>
	  <th>Osoby: </th>
    </tr>
<?php
		$iterations = 0;
		foreach ($rezervace as $item) {
?>
      <tr id="tabulka2">
        <td><?php echo LR\Filters::escapeHtmlText($item[0]) /* line 62 */ ?></td>
        <td><?php echo LR\Filters::escapeHtmlText($item[1]) /* line 63 */ ?></td>
        <td><?php echo LR\Filters::escapeHtmlText($item[2]) /* line 64 */ ?></td>
        <td><?php echo LR\Filters::escapeHtmlText($item[3]) /* line 65 */ ?></td>
		<td><?php echo LR\Filters::escapeHtmlText($item[4]) /* line 66 */ ?></td>
		<td><?php echo LR\Filters::escapeHtmlText($item[5]) /* line 67 */ ?></td>
      </tr>
<?php
			$iterations++;
		}
?>
  </table>
  </div>
    <div class="logout">
    <div class="tlacitko">
		<form action="../src/logout.php" method="POST" name="logout" >
    <button  type="submit" name="logout" class="submit">Odhlásit se</button>
		</form>
    </div>
    </div>
	<footer>
	</footer>
</body>
</html>
<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['item'])) trigger_error('Variable $item overwritten in foreach on line 60');
		
	}

}
